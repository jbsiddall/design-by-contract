Design By Contract for Python
==========================

provides runtime checking of parameters and return types for Python 3.

param decorator
-------------


	@param(name="msg", ptype=str)
	def log(msg): print(msg)
	
	log("Hello, World!'") # allowed
	log(23) # AssertionError
	log(None) # AssertionError


	@param(name="registry", ptype=list)
	@param(name="first_name", ptype=str)
	def person_in_registry(registry, first_name): 
		return first_name in registry
		
	person_in_registry([], "adam") # allowed
	person_in_registry(None, "adam") # AssertionError
	
	
	@param(name="parent", ptype=TreeNode, allow_nulls=True)
	@param(name="value", ptype=object)
	@param(name="children", ptype=list, allow_nulls=True, value_check=lambda children: len(children) <= 2)
	def make_binary_tree_node(parent, value, children):
		children = [] if children is None else children
		return TreeNode(parent, value, children)
		
	make_binary_tree_node(root_node, 23, []) # allowed
	make_binary_tree_node(None, 23, None) # allowed
	make_binary_tree_node(root_node, 23, [node2, node5, node8]) # AssertionError, value_check failed


	@param(name="value, ptype=int, value_check=range(1, 10)) 
	def inverse(value): return 1 / value

	inverse(1) # allowed
	inverse(0) # AssertionError, value_check_failed
	inverse(4.0) # AssertionError
	inverse(10) # AssertionError, value_check failed
		
