import inspect
import decorator

class ValidationError(Exception):
    pass

def param(name, ptype=object, allow_null=False, value_check=None):

        if value_check is None:
            value_check = lambda val: True
        elif hasattr(value_check, "__call__"):
            value_check = value_check
        elif hasattr(value_check, "__iter__"):
            value_check = lambda val: val in value_check
        else:
            raise ValueError("invalid value_check")

        @decorator.decorator
        def wrapper(f, *args, **kwargs):

            arg_names, *_ = inspect.getargspec(f)
            if name not in arg_names:
                raise ValueError("no parameter named " + name)
            index = arg_names.index(name)

            val = args[index]
            if val == None and not allow_null:
                raise AssertionError(
                    "parameter {} not allowed to be null".format(name)
                )

            if val != None:
                fail = False

                if isinstance(ptype, str):
                    classes = [type(val)]
                    while len(classes) != 0:
                        cls = classes[0]
                        del classes[0]
                        if cls.__name__ == ptype:
                            break
                        classes.extend(list(cls.__bases__))
                    else:
                        fail = True

                else:
                    if not isinstance(val, ptype):
                        fail = True

                if fail:
                    raise AssertionError(
                        "parameter '{}' expected type {}, actual type {}".format(
                            name,
                            ptype if isinstance(ptype,str) else ptype.__name__,
                            type(val).__name__
                        )
                    )

            return f(*args, **kwargs)
        return wrapper


class returns:
    def __init__(self, t):
        self.t=t

    def __call__(self, fn):
        def wrapper(*args, **kwargs):
            result = fn(*args, **kwargs)
            if isinstance(result, self.t):
                return result
            else:
                raise TypeError("return type of {} was expected to be {} but actually {}".format(
                        fn.__name__, 
                        self.t.__name__,
                        result.__class__.__name__
                        ))
        wrapper.inner_func = fn
        return wrapper

def generateNamespace(fn, parsedArgs, parsedKwargs):
    parsedArgs = list(parsedArgs)
    parsedKwargs = dict(parsedKwargs)

    while hasattr(fn, "inner_func"):
        fn = fn.inner_func

    (args, varargs, keywords, _)  = inspect.getargspec(fn)
    namespace = {}
    
    for arg in args:
        if len(parsedArgs) != 0:
            namespace[arg] = parsedArgs[0]
            del parsedArgs[0]
        elif arg in parsedKwargs:
            namespace[arg] = parsedKwargs[arg]
            del parsedKwargs[arg]

    namespace[varargs] = parsedArgs
    namespace[keywords] = parsedKwargs

    return namespace

    # namespace = dict(parsedKwargs)
    # for i in range(len(args)):
    #     namespace[functionParameterNames[i]] = args[i]
    # return namespace

# precondition, parameters are refered to as p1, p2, ..., pn
class pre:
    def __init__(self, expression):
        self.expression=expression

    def __call__(self, fn):
        def wrapper(*args, **kwargs):
            namespace = generateNamespace(fn, args, kwargs)

            result = eval(self.expression, globals(), namespace)
            if isinstance(result, bool):
                if result:
                    return fn(*args, **kwargs)
                else:
                    raise ValidationError("precondition '{}' failed for {}".format(
                            self.expression,
                            fn.__name__))
            else:
                raise TypeError("precondition must be a predicate of type bool, not {}".format(
                        result.__class__.__name__
                        ))
        wrapper.inner_func = fn
        return wrapper
                
            
# postcondition, parameters are refered to as p1, p2, ..., pn, result is r
class post:
    def __init__(self, expression):
        self.expression=expression

    def __call__(self, fn):
        def wrapper(*args, **kwargs):

            result = fn(*args, **kwargs)
            namespace = generateNamespace(fn, args, kwargs)
            namespace["result"] = result

            predicateResult = eval(self.expression, globals(), namespace)
            if isinstance(predicateResult, bool):
                if predicateResult:
                    return result
                else:
                    raise ValidationError("postcondition '{}' failed for {}".format(
                            self.expression,
                            fn.__name__))
            else:
                raise TypeError("postcondition must be a predicate of type bool, not {}".format(
                        predicateResult.__class__.__name__
                        ))
        wrapper.inner_func = fn
        return wrapper

    
